import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by Evgeniy on 16.02.2017.
 */
public class LambdaExample {

    public static void main(String[] args) {

        BiFunction<Person, String, Boolean> personFirstNameEquals = (person, s) -> person.getFirstName().equals(s);

        BiFunction<Person, String, Boolean> personLastNameEquals = (person, s) -> person.getLastName().equals(s);

        BiFunction<Person, Integer, Boolean> personAgeEquals = (person, age) -> person.getAge().equals(age);

        BiFunction<Person, Integer, Boolean> personAgeGreaterThan = (person, age) -> person.getAge() > age;

        BiFunction<Person, Integer, Boolean> personAgeLessThan = (person, age) -> person.getAge() < age;

        BiFunction<Person, Integer, Boolean> personAgeGreaterThanOrEqual = (person, age) -> person.getAge() >= age;

        BiFunction<Person, Integer, Boolean> personAgeLessThanOrEqual = (person, age) -> person.getAge() <= age;

        BiFunction<Person, Gender, Boolean> personGenderEquals = (person, gender) -> person.getGender().equals(gender);

        BiFunction<Person, Integer, Person> personSetAge = (person, age) -> new Person(person).setAge(age);

        BiFunction<Person, String, Person> personSetFirstName = (person, s) -> new Person(person).setFirstName(s);

        BiFunction<Person, String, Person> personSetLastName = (person, s) -> new Person(person).setLastName(s);

        BiFunction<Person, Gender, Person> personSetGender = (person, gender) -> new Person(person).setGender(gender);

        BiFunction<List<Person>, Predicate<Person>, List<Person>> filter = (personList, personBooleanFunction) -> personList
                .stream()
                .filter(personBooleanFunction)
                .collect(Collectors.toList());

        Predicate<Person> isAdult = person -> person.getAge() >= 18;

        Predicate<Person> isMale = person -> person.getGender().equals(Gender.MALE);

        Predicate<Person> isAdultMale = isAdult.and(isMale);

        List<Person> personList = new ArrayList<>();
        personList.add(new Person("Bruce", "Banner", 38, Gender.MALE));
        personList.add(new Person("Natasha", "Romanoff", 27, Gender.FEMALE));
        personList.add(new Person( "Peter", "Parker", 16, Gender.MALE));

        System.out.println("Before filtering");
        personList
                .stream()
                .forEach(System.out::println);

        List<Person> newPersonList = filter.apply(personList, isAdultMale);

        System.out.println("After filtering");
        newPersonList
                .stream()
                .forEach(System.out::println);



    }

}
